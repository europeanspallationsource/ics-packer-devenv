#!/bin/bash

ISO=CentOS-7-x86_64-DVD-1708.iso
if [[ ! -f CentOS_7/${ISO} ]]
then
  echo "Downloading the ${ISO} image..."
  curl --fail -o CentOS_7/${ISO} https://artifactory.esss.lu.se/artifactory/swi-pkg/centos/${ISO}
fi

# Create version file for devenv-firstboot
echo "$DEVENV_VERSION" > kickstart/devenv_version

# Create bootstrap script to be run on a clean CentOS install
mkdir -p build
sed "s/^DEVENV_VERSION=.*/DEVENV_VERSION=$DEVENV_VERSION/" kickstart/devenv-firstboot > build/bootstrap-devenv

docker build -t centos-extlinux docker
docker run --rm -v /dev:/dev -v $(pwd):/build --privileged centos-extlinux ./create_ks_img
