#!/bin/bash

# Script run from devenv-config menu when performing an update of the DM
# It's packaged in the ics-ans-devenv-${DEVENV_VERSION}.tar.gz archive
# and allows to perform extra actions before to run the Ansible plyabook
# It's run as root

# Remove old "all" group_vars
# Group changed to "devenv"
rm -f /etc/ansible/group_vars/all

# Install required version of ansible-playbook and ansible-galaxy PEX files
ANSIBLE_VERSION=$(cat /etc/ansible/.ansible_version)
for exe in ansible-playbook ansible-galaxy
do
  curl --fail -o /usr/local/bin/${exe} https://artifactory.esss.lu.se/artifactory/swi-pkg/ansible-releases/${ANSIBLE_VERSION}/${exe}
  chmod a+x /usr/local/bin/${exe}
done

# Update all packages
yum update -y
exit 0
