pipeline {
    agent { label 'packer' }
    environment {
        VAGRANTCLOUD_API_URL = 'https://app.vagrantup.com/api/v1'
        VAGRANTCLOUD_TOKEN = credentials('VAGRANTCLOUD_TOKEN')
    }

    stages {
        stage('Validate VM template') {
            steps {
                slackSend (color: 'good', message: "STARTED: <${env.BUILD_URL}|${env.JOB_NAME} [${env.BUILD_NUMBER}]>")
                sh 'git fetch --tags'
                script {
                    DEVENV_VERSION = sh(returnStdout: true, script: 'git describe').trim()
                    GIT_TAG = sh(returnStdout: true, script: 'git describe --exact-match || true').trim()
                }
                echo "DEVENV_VERSION: $DEVENV_VERSION"
                echo "GIT_TAG: $GIT_TAG"
                sh 'packer validate devenv-vm.json'
            }
        }
        stage('Build VM') {
            environment {
                DEVENV_VERSION = "$DEVENV_VERSION"
            }
            steps {
                ansiColor('xterm') {
                    sh 'packer build devenv-vm.json'
                }
            }
        }
        stage('Test VM') {
            steps {
               sh './run_tests'
            }
        }
        stage('Upload VM artifacts') {
            when {
                expression { "$DEVENV_VERSION" == "$GIT_TAG" }
            }
            steps {
                script {
                    def server = Artifactory.server 'artifactory.esss.lu.se'
                    def uploadSpec = """{
                      "files": [
                        {
                          "pattern": "build/*",
                          "target": "devenv/devenv-releases/${DEVENV_VERSION}/"
                        },
                        {
                          "pattern": "config/Vagrantfile",
                          "target": "devenv/devenv-releases/${DEVENV_VERSION}/"
                        }
                      ]
                    }"""
                    server.upload(uploadSpec)
                }
            }
        }
        stage('Release VM') {
            when {
                expression { "$DEVENV_VERSION" == "$GIT_TAG" }
            }
            environment {
                DEVENV_VERSION = "$DEVENV_VERSION"
            }
            steps {
                /* create new version */
                sh 'curl ${VAGRANTCLOUD_API_URL}/box/esss/devenv-7/versions -X POST -H "X-Atlas-Token: $VAGRANTCLOUD_TOKEN" -d version[version]="$DEVENV_VERSION"'
                /* create new provider */
                sh 'curl ${VAGRANTCLOUD_API_URL}/box/esss/devenv-7/version/$DEVENV_VERSION/providers -X POST -H "X-Atlas-Token: $VAGRANTCLOUD_TOKEN" -d provider[name]="virtualbox"'
                /* get path to upload the box */
                script {
                    upload_path = sh(
                        returnStdout: true,
                        script: 'curl "${VAGRANTCLOUD_API_URL}/box/esss/devenv-7/version/${DEVENV_VERSION}/provider/virtualbox/upload" -H "X-Atlas-Token: $VAGRANTCLOUD_TOKEN" | jq -r .upload_path').trim()
                }
                sh "curl -X PUT --upload-file build/esss-devenv-7.box ${upload_path}"
                /* release the box version */
                sh 'curl ${VAGRANTCLOUD_API_URL}/box/esss/devenv-7/version/${DEVENV_VERSION}/release -X PUT -H "X-Atlas-Token: ${VAGRANTCLOUD_TOKEN}"'
            }
        }
        stage('Build bootable USB image') {
            environment {
                DEVENV_VERSION = "$DEVENV_VERSION"
            }
            steps {
                dir('physical') {
                    sh './build_img.sh'
                }
            }
        }
        stage('Upload DM bootstrap script') {
            when {
                expression { "$DEVENV_VERSION" == "$GIT_TAG" }
            }
            steps {
                script {
                    def server = Artifactory.server 'artifactory.esss.lu.se'
                    def uploadSpec = """{
                      "files": [
                        {
                          "pattern": "physical/build/bootstrap-devenv",
                          "target": "devenv/devenv-releases/${DEVENV_VERSION}/"
                        }
                      ]
                    }"""
                    server.upload(uploadSpec)
                }
            }
        }
        stage('Upload bootable USB image') {
            when {
                expression { "$DEVENV_VERSION" == "$GIT_TAG" }
            }
            steps {
                script {
                    def server = Artifactory.server 'artifactory.esss.lu.se'
                    def uploadSpec = """{
                      "files": [
                        {
                          "pattern": "physical/build/devenv_ks.${DEVENV_VERSION}.img",
                          "target": "devenv/devenv-releases/${DEVENV_VERSION}/"
                        }
                      ]
                    }"""
                    server.upload(uploadSpec)
                }
            }
        }
    }

    post {
        always {
            /* clean up the workspace */
            deleteDir()
        }
        failure {
            slackSend (color: 'danger', message: "FAILED: <${env.BUILD_URL}|${env.JOB_NAME} [${env.BUILD_NUMBER}]>")
        }
        success {
            slackSend (color: 'good', message: "SUCCESSFUL: <${env.BUILD_URL}|${env.JOB_NAME} [${env.BUILD_NUMBER}]>")
        }
    }

}
